import React,{ useEffect} from 'react'
import { Switch ,Route } from 'react-router-dom'

// pages
import Home from './pages/Home'
import About from './pages/About/'
import Portfolio from './pages/Portfolio'
import Blog from './pages/Blog'
import Contact from './pages/Contact'
// images to props
import link from './img/ikona_link_ciemna.png'
import bitbucket from './img/ikona_bitbucket_ciemna.png'
import dev from './img/ikona_DEV_ciemna.png'
import easycode from './img/easy_code_button_grey.png'
import person from './img/man.png'
import marker from './img/sygnet_kwadraty_znacznik.png'
import p_line from './img/_percentage_line_100_mine.png'
import el_bgh_1 from './img/element_graficzny_tła_01.png'
import el_bgh_2 from './img/element_graficzny_tła_02.png'
import el_bgv_3 from './img/element_graficzny_tła_03_pionowy.png'
import el_bgv_4 from './img/element_graficzny_tła_04_pionowy.png'
import el_bgv_5 from './img/element_graficzny_tła_05_pionowy.png'
// tools
import reactIcon from './img/_react_icon_s.png'
import webpackIcon from './img/webpack_icon.png'
import expressIcon from './img/express_icon.png'
import styledIcon from './img/styled_components_icon.png'
import reduxIcon from './img/redux_icon_s.png'
import flexIcon from './img/flexbox_icon_s.png'
import otherIcon from './img/other_tool_programm_icon_sygnet.png'
// websites
import website_1 from './img/website-1.png'
import website_2 from './img/website-2.png'
import website_3 from './img/website-3.png'
import website_4 from './img/website-4.png'
import website_5 from './img/website-5.png'
import website_6 from './img/website-6.png'
// blog images
import blog_1 from './img/blog-post-1.jpg'
import blog_2 from './img/blog-post-2.jpg'

const images = {
  link,
  bitbucket,
  dev,
  easycode,
  person,
  marker,
  p_line,
  el_bgh_1,
  el_bgh_2,
  el_bgv_3,
  el_bgv_4,
  el_bgv_5
}

const icons = {
  reactIcon,
  webpackIcon,
  expressIcon,
  styledIcon,
  reduxIcon,
  flexIcon,
  otherIcon
}

const websites = {
  website_1,
  website_2,
  website_3,
  website_4,
  website_5,
  website_6
}

const blogImages = {
  blog_1,
  blog_2
}
const App = () => {
  useEffect(()=>{
    let body = document.querySelector('body')
        if(window.location.href !== window.location.href + "portfolio"){
          body.style.overflow = 'hidden'
        }else{
          body.style.overflow = 'hidden'
          body.style.overflowY = 'visible'
        }
     
    })
  return (
    <div className="main-container">
      <Switch>
        <Route exact path='/'>
          <Home 
            images = {images}
          />
          
        </Route>
        <Route exact path='/about'>
          <About 
            images = {images} icons={icons}
          />
        </Route>
        <Route exact path='/portfolio'>
          <Portfolio
            images = {images} websites={websites}
          />
        </Route>
        <Route exact path='/blog'>
          <Blog 
            images = {images} blogImages={blogImages}
          />
        </Route>
        <Route exact path='/contact'>
          <Contact 
            images = {images} blogImages = {blogImages}
          />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
