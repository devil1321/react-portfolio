const NavbarLogic = () =>{
const body = document.querySelector('body')
const navBtn = document.querySelector('.nav-btn')
const navList = document.querySelector('.nav__list')
const navItemsDOM = document.querySelectorAll('.nav__item')
const overlay = document.querySelector('.overlay')
function toggleMenu(){
    if(navBtn.classList.contains('open')){
        navBtn.classList.remove('open')
        let navItems = [...navItemsDOM]
        navItems.forEach(item=>{
            item.style.opacity = 0      
        })
        navList.style.height = 0
        setTimeout(()=>{
            overlay.style.opacity = '0'
            overlay.style.visibility = 'hidden'
        },300)
    }else{
        body.style.overflowY = 'visible'
        let navItems = [...navItemsDOM] 
        navBtn.classList.add('open')
        if(window.innerWidth <= 540){
            navList.style.height = '250px'
        }
        else{
            navList.style.height = '300px'
        }
        let time = 300
        setTimeout(()=>{
            navItems.forEach(item=>{
                setTimeout(()=>{
                    item.style.opacity = 1      
                },time+=200)
            })
        },200)
        setTimeout(()=>{
            overlay.style.visibility = 'visible'
            overlay.style.opacity = '1'
        },300)
        
    }
}

navBtn.addEventListener('click',toggleMenu)

function mediaQuery(){

    let navItems = [...navItemsDOM]
    if(window.innerWidth >= 640  || window.innerWidth === 720){
        navList.style.height = '100%'  
        navItems.forEach(item=>item.style.opacity = 1)
        if(window.innerWidth >= 640 || window.innerWidth === 720){
            if(navBtn.classList.contains('open')){
                navList.style.height =  '300px'
                if(window.innerWidth === 720){
                    navList.style.height =  '320px'
                }
            }else{
                navList.style.height =  '0px'
            }
        }
        if(window.innerWidth === 823){       
            navList.style.height = '0%'
            if(navBtn.classList.contains('open')){
                navList.style.height =  '300px'
            }     
        }    
    }

    else{
        if(navBtn.classList.contains('open')){
            navList.style.height =  '250px'
            navItems.forEach(item=>item.style.opacity = 1)
        }
        else{
            navList.style.height = '0px'
            navList.padding = '0px'
            navItems.forEach(item=>item.style.opacity = 0)
        }
        
    }
    if(window.innerWidth === 768 && !navBtn.classList.contains('open')){
        navItems.forEach(item=>{
            item.style.opacity = 0      
        })
    }
}
window.addEventListener('resize',mediaQuery)

if (navigator.appVersion.indexOf("Chrome/") != -1) {
   const asideFooterText = document.querySelectorAll('.article__footer-inner-text')
   asideFooterText.forEach(text=>{
       text.style.padding = '10px 20px 10px 0px'
   })
}
}
export default NavbarLogic
