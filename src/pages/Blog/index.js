import React from 'react'
import { Link } from 'react-router-dom'
import Title from '../../components/Title'
import BlogPost from '../../components/BlogPost'
import Navbar from '../../components/Navbar'
import './blog.css'
const Blog = ({images,blogImages}) => {
    const { person ,el_bgh_1, el_bgv_3 } = images
    const { blog_1, blog_2 } = blogImages
    return (
        <div className="blog">
        <div className="container d-flex">
        <div className="overlay"></div>
        <Navbar />
        <div className="article d-flex j-c-sb">
            <div className="article__inner">
                <div className="article__text-1">
                    <Title title="Blog" subtitle="My subjective point of programming" />
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto ipsum nam rem voluptates dolores inventore accusantium.</p>
                </div>
                <BlogPost bandge="New" img={blog_1} title="Blog post 02" subtitle="Secondary Title" date="author. 12/09/2020" text="Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste neque enim similique, corrupti ea maxime quo?" />
                <div className="article__line"></div>
                <BlogPost bandge="Most Popular" img={blog_2} title="Blog post 01" subtitle="Secondary Title" date="author. 12/19/2020" text="Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste neque enim similique, corrupti ea maxime quo?" />
                <div className="article__image-1-wrapper">
                    <img className="article__image-1" src="./img/element_graficzny_tła_02.png" alt="element_graficzny_tła_02"/>
                </div>
            </div>
            <div className="article__dividers-wrapper">
                <img className="article__divider-1" src={el_bgh_1} alt="element-tla-1"/>
                <img className="article__divider-2" src={el_bgv_3} alt="element-tla-3"/>
            </div>
            <div className="article__aside">
                <img className="article__aside-image" src={person} alt="man" width="100%"/>
                <div className="article__categories">
                    <h2>Categories</h2>
                    <div className="article__categories-inner">
                        <Link to="#" >Latest</Link>
                        <div className="link-icon"></div>
                    </div>
                    <div className="article__categories-inner">
                        <Link to="#" >Theme 01</Link>
                        <div className="link-icon"></div>
                    </div>
                    <div className="article__categories-inner">
                        <Link to="#" >Tips & Tricks</Link>
                        <div className="link-icon"></div>
                    </div>
                    <div className="article__categories-inner">
                        <Link to="#" >Off top</Link>
                        <div className="link-icon"></div>
                    </div>
                    <div className="article__categories-inner">
                        <Link to="#" >FAQ on freelance programming</Link>
                        <div className="link-icon"></div>
                    </div>
                </div>
                <div className="article__about">
                    <h2>Read about</h2>
                    <Link to="#">#freelance</Link>
                    <Link to="#">#online courses</Link>
                    <Link to="#">#networking</Link>
                    <Link to="#">#home office</Link>
                    <Link to="#">#programming</Link>
                    <Link to="#">#blog</Link>
                </div>
            </div>
        </div>
    </div>
    </div>
    )
}

export default Blog
