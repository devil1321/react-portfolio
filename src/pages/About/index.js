import React from 'react'
import Navbar from '../../components/Navbar'
import Title from '../../components/Title'
import ArticleSkill from '../../components/ArticleSkill'
import ArticleTool from '../../components/ArticleTool'
import './about.css'
const About = ({images,icons}) => {
    const {bitbucket,dev,marker,p_line,el_bgh_1,el_bgh_2,el_bgv_3,el_bgv_5} = images
    const {reactIcon, webpackIcon, expressIcon, styledIcon, reduxIcon, flexIcon, otherIcon} = icons
    return (
        <div className="about">
            <Navbar />
            <div className="container d-flex">
        <div className="overlay"></div>
        <div className="article">
            <div className="article__main-inner-wrapper d-flex j-c-sb a-i-c">
                <div className="article__left">
                    <div className="article__left-inner-wrapper">
                        <img className="article__left-top-image" src={el_bgh_2} alt="element-tla-2" />
                        <div className="article__left-text-1">
                            <Title title="About Me" subtitle="All about Techy"/>
                            <p>Lorem ipsum dolor sit amet.</p>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quaerat, porro ab repudiandae recusandae animi iste sunt dolor ex repellendus! Eius eligendi earum, neque repellat ratione quas quidem deleniti, id optio magni totam
                                architecto commodi iure reprehenderit rerum dignissimos veniam exercitationem quod! Maxime, similique deserunt reprehenderit repudiandae odio assumenda sequi temporibus.</p>
                        </div>
                        <div className="article__left-icons d-flex a-i-c">
                            <p>See My Works on Bitbucket and Dev</p>
                            <div className="article__left-icons-inner d-flex a-i-c">
                                <a href="#">
                                    <img className="article__left-icon" src={bitbucket} alt="bitbucket" />
                                </a>
                                <a href="#">
                                    <img className="article__right-icon" src={dev} alt="dev" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="article__left-text-2">
                        <h3>My interests</h3>
                        <p>Automotive</p>
                        <p>New Technologies</p>
                        <p>Sport</p>
                        <p>IT</p>
                    </div>
                </div>
                <div className="article__divider">
                    <img className="article__divider-image" src={el_bgv_3} alt="element-tla-3" />
                </div>
                <div className="article__right">
                    <img className="article__right-image-horizontal" src={el_bgh_1} alt="element-tla-1" />
                    <div className="article__right-text-1">
                        <Title title="My skills" subtitle="Secondary Title"/>
                        <p>Lorem ipsum architecto modi quisquam, pariatur consectetur molestiae officia ut nam ratione quam dicta placeat natus, maiores
                            nulla repellendus fuga totam nobis. Tenetur, esse voluptatem!</p>
                    </div>
                    <div className="article__right-skills-main">
                        <ArticleSkill percentage='80%' line={p_line} language="PHP"/>
                        <ArticleSkill percentage='100%' line={p_line} language="JS"/>
                        <ArticleSkill percentage='100%' line={p_line} language="HTML"/>
                        <ArticleSkill percentage='60%' line={p_line} language="NODEJS"/>
                        <ArticleSkill percentage='100%' line={p_line} language="CSS"/>
                        <ArticleSkill percentage='80%' line={p_line} language="GO"/>
                        <ArticleSkill percentage='100%' line={p_line} language="NEXT"/>
                    </div>
                </div>
            </div>
            <div className="article__bottom-line"></div>
            <div className="article__footer d-flex j-c-sb a-i-c">
                <div className="article__footer-left">
                    <img className="article__logo" src={marker} alt="znacznik"/>
                    <div className="article__footer-text-1">
                        <h2>Tools</h2>
                        <h3>Programms I use</h3>
                        <p>Essentials to make the work done.</p>
                    </div>
                </div>
                <div className="article__footer-right">
                    <div className="article__tools d-flex j-c-sa a-i-c">
                        <ArticleTool img={reactIcon} tool="React" version="16.6.3"/>
                        <ArticleTool img={webpackIcon} tool="Webpack" version="4.19.1"/>
                        <ArticleTool img={expressIcon} tool="Express" version="4.06.4"/>
                        <ArticleTool img={styledIcon} tool="Styled Components" version="4.16.4"/>
                        <ArticleTool img={reduxIcon} tool="Redux" version="4.16.4"/>
                        <ArticleTool img={flexIcon} tool="Flexbox" version="4.0.1"/>
                        <ArticleTool img={otherIcon} tool="Program" version="5.21"/>
                        <ArticleTool img={otherIcon} tool="Program" version="5.2.2"/>
                    </div>
                    <div className="article__footer-image-wrapper">
                        <img className="article__footer-image" src={el_bgv_5} alt="element-tla-5" />
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    )
}

export default About
