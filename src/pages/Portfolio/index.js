import React from 'react'
import { Link } from 'react-router-dom'
import Navbar from '../../components/Navbar'
import Title from '../../components/Title'
import PortfolioItem from '../../components/PortfolioItem'
import './portfolio.css'
const Portfolio = ({images,websites}) => {
    const { link, bitbucket, person, marker, el_bgh_1, el_bgh_2 } = images
    const { website_1, website_2, website_3, website_4, website_5, website_6 } = websites 
    return (
        <div className="portfolio">
            <Navbar />
            <div className="container d-flex">
        <div className="overlay"></div>
        <div className="article">
            <div className="article__top d-flex a-i-c">
                <div className="article__top-left">
                    <Title title="Portfolio" subtitle="See the works I enjoyed working on" />
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequatur quas consequuntur recusandae asperiores. Vitae, nihil.</p>
                    <div className="article__category d-flex j-c-sb a-i-c">
                        <Link to="#">Category 01</Link>
                        <Link to="#">Category 02</Link>
                        <Link to="#">Category 03</Link>
                        <Link to="#">Category 04</Link>
                    </div>
                </div>
                <div className="article__divider-wrapper">
                    <img className="article__divider" src={el_bgh_1} alt="element-tla-1" />
                </div>
                <div className="article__right">
                    <img src={person} alt="main-image" className="article__right-main-image" />
                </div>
            </div>
            <div className="article__websites d-flex j-c-sb a-i-c f-w">
                <PortfolioItem icons = {{bitbucket,link}} website={website_1}/>
                <PortfolioItem icons = {{bitbucket,link}} website={website_2}/>
                <PortfolioItem icons = {{bitbucket,link}} website={website_3}/>
                <PortfolioItem icons = {{bitbucket,link}} website={website_4}/>
                <PortfolioItem icons = {{bitbucket,link}} website={website_5}/>
                <PortfolioItem icons = {{bitbucket,link}} website={website_6}/>
            </div>
            <div className="article__bottom-image-wrapper">
                <img className="article__bottom-image" src={el_bgh_2} alt="element-tla-2" />
            </div>
        </div>
    </div>
        </div>
    )
}

export default Portfolio
