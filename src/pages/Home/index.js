import React from 'react'
import { Link } from 'react-router-dom'
import Navbar from '../../components/Navbar'
import Title from '../../components/Title'
import './main.css'
const Home = ({images}) => {
    const {bitbucket,dev,easycode,person,marker,el_bgh_1,el_bgh_2,el_bgv_3,el_bgv_4,el_bgv_5} = images
    return (
        <div className="home">
            <div className="container d-flex">
            <div className="overlay"></div>
            <Navbar />
            <div className="article d-flex">
                <div className="article__left">
                    <div className="article__text-1">
                        <Title title="Hi! My Name is Dominik Stępień" subtitle="Passionate software engineer"/>
                        <p>Passionate Techy and Tech autor with 5 years</p>
                        <p> of valuable experience within field of software engineering.</p>
                        <p className="article__larger-text">See my works on Bitbucket and DEV</p>
                    </div>
                    <div className="article__icons d-flex j-c-e">
                        <Link to="#">
                            <img className="bitbucket-icon" src={bitbucket} alt="bitbucket" />
                        </Link>
                        <Link to="#">
                            <img className="dev-icon" src={dev} alt="dev" />
                        </Link>
                    </div>
                    <div className="article__line"></div>
                    <div className="article__horizontal-divider">
                        <img src={el_bgh_2} alt="element-tla-2" />
                    </div>
                    <div className="article__text-2">
                        <p>author: Dominik Stępień</p>
                        <p>username: @Dominik Stepien</p>
                        <p>description: Univesity Graduate | Software Engineer</p>
                        <p>repository type: Open-source</p>
                        <p>ulr: bitbucket.com/Dominik Stepien</p>
                    </div>
                    <div className="article__left-footer d-flex j-c-sb a-i-c">
                        <p>Ukonczylem kurs Easy Code</p>
                        <Link to="#" className="btn-easy-code">
                            <img src={easycode}alt="easy-code" />
                        </Link>
                    </div>
                </div>
                <div className="article__divider-vertical">
                    <img src={el_bgv_3} alt="element-tla-3" />
                </div>
                <div className="article__right">
                    <img className="article__right-image-1" src={el_bgh_1} alt="element-tla-1" />
                    <img className="article__right-main-image" src={person} alt="person"/>
                    <div className="article__right-vertical-divider">
                        <img src={el_bgv_4} alt="element-tla-4 " />
                    </div>
                    <div className="article__text-3">
                        <img className="article__logo" src={marker} alt="znacznik " />
                        <div>
                            <h2>I am freelancer</h2>
                            <h3>So you can ask me to join your projects</h3>
                            <p>You can ask me to join the conference or hire me to work with you on some project. Please send me a shor brief or contact me via e-mail. I will be happy to help you</p>
                        </div>
                        <div className="article__buttons d-flex j-c-e a-i-c">
                            <Link to="#">
                                <button className="btn download-cv ">Download CV</button>
                            </Link>
                            <Link to="#">
                                <button className="btn hire-me ">Hire Me</button>
                            </Link>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    )
}

export default Home
