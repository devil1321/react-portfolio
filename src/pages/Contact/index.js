import React from 'react'
import { Link } from 'react-router-dom'
import Navbar from '../../components/Navbar'
import './contact.css'
const Contact = ({images,blogImages}) => {
    const { link,bitbucket,dev,easycode,person,marker,p_line,el_bgh_1,el_bgh_2,el_bgv_3,el_bgv_4,el_bgv_5 } = images
    const { blog_1 } = blogImages
    return (
        <div className="contact">
            <Navbar />
            <div className="container d-flex">
        <div className="overlay"></div>
        <div className="article d-flex">
            <div className="article__left">
                <div className="article__form-wrapper">
                    <img className="article__logo" src={marker} alt="znacznik" />
                    <form className="article__form" action="" mehtod="" encType="application/x-www-form-urlencoded">
                        <h2>Contact Me</h2>
                        <p>If you are willing to work with me, contact me. I can join our conference to serve you with my engineering experience.</p>
                        <label className="article__label" htmlFor="name">Please, enter your name</label>
                        <input className="article__input" type="text" id="name" name="name" />
                        <label className="article__label" htmlFor="emai">E-mail adress</label>
                        <input className="article__input" type="text" id="email" name="email" />
                        <label className="article__label" htmlFor="comment">Your message</label>
                        <textarea className="article__textarea" name="comment" id="comment" cols="30" rows="1"></textarea>
                        <div className="article__submit">
                            <button type="submit" className="article__submit-button">Send</button>
                        </div>
                    </form>
                </div>
                <div className="article__bottom-text">
                    <p>author: Dominik Stępień</p>
                    <p>username: @Dominik Stepien</p>
                    <p>description: Univesity Graduate | Software Engineer</p>
                    <p>repository type: Open-source</p>
                    <p>ulr: bitbucket.com/Dominik Stepien</p>
                </div>
                <div className="article__bottom-image-wrapper">
                    <img className="article__bottom-image" src={el_bgh_2} alt="element-tla-2" />
                </div>
            </div>
            <div className="article__divider-wrapper">
                <img className="article__divider" src={el_bgv_3} alt="element-tla-3" />
            </div>
            <div className="article__aside">
                <div className="article__blog-post">
                    <div className="article__blog-post-text first">
                        <p>New</p>
                    </div>
                    <div className="article__blog-post-text second">
                        <p>Blog post 01</p>
                    </div>
                    <img src={blog_1} alt="blog-post-image" />
                </div>
                <div className="article__text">
                    <h2>Read</h2>
                    <div className="article__text-inner">
                        <Link to="#" href="#">Blog post 01</Link>
                        <div className="link-icon"></div>
                    </div>
                    <div className="article__text-inner">
                        <Link to="#" href="#">Blog post 02</Link>
                        <div className="link-icon"></div>
                    </div>
                    <div className="article__text-inner">
                        <Link to="#" href="#">Blog post 03</Link>
                        <div className="link-icon"></div>
                    </div>
                </div>
                <div className="article__aside-footer">
                    <img className="article__logo" src={marker} alt="znacznik " />
                    <h2>Partners</h2>
                    <div className="article__footer-inner-text">
                        <Link to="#" href="#">Easy coder 03</Link>
                        <div className="link-icon"></div>
                    </div>
                    <div className="article__footer-inner-text">
                        <Link to="#" href="#">TECHOOO Techy Vlog 01</Link>
                        <div className="link-icon"></div>
                    </div>
                </div>
                <div className="article__aside-image-wrapper">
                    <img className="article__aside-image" src={el_bgv_4} alt="element-tla-4" />
                </div>
            </div>
        </div>
    </div>
        </div>
    )
}

export default Contact
