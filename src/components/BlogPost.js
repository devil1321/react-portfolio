import React from 'react'

const BlogPost = ({bandge,img,title,subtitle,date,text}) => {
    return (
        <React.Fragment>
             <div className="article__blog-post">
                    <div className="article__blog-post-category d-flex j-c-c a-i-c">
                        <h6>{bandge}</h6>
                    </div>
                    <div className="article__blog-post-inner d-flex j-c-sb a-i-c">
                        <div className="article__blog-post-image">
                            <a href="#">
                                <img src={img} alt="blog-post-image-1" width="100%"/>
                            </a>
                        </div>
                        <div className="article__blog-post-text">
                            <a href="#">
                                <h2>{title}</h2>
                            </a>
                            <h3>{subtitle}</h3>
                            <h6>{date}</h6>
                            <p>{text}</p>
                            <div className="article__blog-post-link d-flex j-c-c a-i-c">
                                <a href="">Read the article</a>
                                <div className="link-icon"></div>
                            </div>
                        </div>
                    </div>
                </div>
        </React.Fragment>
    )
}

export default BlogPost
