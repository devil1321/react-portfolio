import React from 'react'

const ArticleTool = ({img,tool,version}) => {
    return (
        <React.Fragment>
             <div className="article__icon-wrapper">
                <img src={img} alt="react" />
                <p>{tool}</p>
                <p>{version}</p>
            </div>
        </React.Fragment>
    )
}

export default ArticleTool
