import React from 'react'

const ArticleSkill = ({percentage,line,language}) => {
    return (
        <React.Fragment>
            <div className="article__right-skills">
                <div className="article__right-skill">
                    <p>{percentage}</p>
                        <div className="article__right-skill-image-wrapper">
                            <img className="article__right-skill-percentage-line" src={line} alt="percentage"/>
                        </div>
                    </div>
                <p>{language}</p>
            </div>
        </React.Fragment>
    )
}

export default ArticleSkill
