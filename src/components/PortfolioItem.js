import React from 'react'
const PortfolioItem = ({website,icons}) => {
    return (
        <React.Fragment>
             <div className="article__website-image-wrapper">
                    <a href="#">
                        <img src={website} alt="website-1" />
                    </a>
                    <div className="article__website-icons">
                        <p>See more:</p>
                        <a href="#">
                            <img src={icons.bitbucket} alt="bitbucket-icon" />
                        </a>
                        <a href="#">
                            <img src={icons.link} alt="link-icon" />
                        </a>
                    </div>
                </div>
        </React.Fragment>
    )
}

export default PortfolioItem
