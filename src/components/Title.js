import React from 'react'
import logo from '../img/sygnet_kwadraty_znacznik.png'
const Title = ({title,subtitle}) => {
    return (
        <React.Fragment>
            <img className="article__logo" src={logo} alt="znacznik" />
            <h1>{title}</h1>
            <h3>{subtitle}</h3>
        </React.Fragment>
    )
}

export default Title
