import React,{useEffect} from 'react'
// navbar logic
import NavbarLogic from '../js/main.js'
import { Link } from 'react-router-dom'
import logoIcon from "../img/sygnet_kwadraty_.png"
import aboutIcon from "../img/ikona_about_me.png"
import portfolioIcon from "../img/ikona_portfolio.png"
import blogIcon from "../img/ikona_blog.png"
import contactIcon from "../img/ikona_mailing_contact.png"
import bitbucketIcon from "../img/ikona_bitbucket.png"
import devIcon from "../img/ikona_DEV.png"
import twitterIcon from "../img/twitter_icon.png"
import facebookIcon from "../img/facebook_icon.png"
import linkedinIcon from "../img/linkedin_icon.png"

const Navbar = () => {
    useEffect(()=>{
        NavbarLogic()
    })
    return (
        <div className="col-1 nav ">
        <Link to="/">
                <img className="nav__logo" src={logoIcon} alt="logo" />
            </Link>
            <div className="nav-btn">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <ul className="nav__list">
                <li className="nav__item menu-md">
                    <Link to="/about">
                        <p className="nav__item-text">About</p>
                        <img src={aboutIcon} alt="about" />
                    </Link>
                </li>
                <li className="nav__item menu-md">
                    <Link to="/portfolio">
                        <p className="nav__item-text">Portfolio</p>
                        <img src={portfolioIcon} alt="portfolio" />
                    </Link>
                </li>
                <li className="nav__item menu-md">
                    <Link to="/blog">
                        <p className="nav__item-text">Blog</p>
                        <img src={blogIcon} alt="blog" />
                    </Link>
                </li>
                <li className="nav__item menu-md">
                    <Link to="/contact">
                        <p className="nav__item-text">Contact</p>
                        <img src={contactIcon} alt="contact" />
                    </Link>
                </li>
                <div className="nav__line"></div>
                <li className="nav__item">
                    <Link to="#">
                        <img src={bitbucketIcon} alt="bitbucket" />
                    </Link>
                </li>
                <li className="nav__item">
                    <Link to="#">
                        <img src={devIcon} alt="dev" />
                    </Link>
                </li>
                <li className="nav__item">
                    <Link to="#">
                        <img src={twitterIcon} alt="twitter" />
                    </Link>
                </li>
                <li className="nav__item">
                    <Link to="#">
                        <img src={facebookIcon} alt="facebook" />
                    </Link>
                </li>
                <li className="nav__item">
                    <Link to="#">
                        <img src={linkedinIcon} alt="linkedin" />
                    </Link>
                </li>
            </ul>
        </div>
    )
}

export default Navbar
